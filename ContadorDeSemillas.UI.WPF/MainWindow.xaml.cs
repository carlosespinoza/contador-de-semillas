﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using AForge.Video;
using AForge.Video.DirectShow;
using System.Drawing;
using System.ComponentModel;
using System.IO;
using System.Drawing.Imaging;
using System.Threading;
using Tools;
using System.Windows.Threading;

namespace ContadorDeSemillas.UI.WPF
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private FilterInfoCollection _videoDevices;
        private VideoCaptureDevice _videoSource;
        private BitmapImage _frameActual;
        private PuertoSerie puertoSerie;
        private string comando;
        private DispatcherTimer timer;
        private DispatcherTimer timerLog;
        string log;
        public MainWindow()
        {
            InitializeComponent();
            _videoDevices = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            cmbOrigenes.ItemsSource = _videoDevices;
            cmbOrigenes.DisplayMemberPath = "Name";
            _videoSource = new VideoCaptureDevice();
            puertoSerie = new PuertoSerie();
            puertoSerie.DatoObtenido += PuertoSerie_DatoObtenido;
            puertoSerie.Evento += PuertoSerie_Evento;
            cmbPuerto.ItemsSource = puertoSerie.PuertosDisponibles();
            timer = new DispatcherTimer();
            comando = "";
            timer.Tick += Timer_Tick;
            timer.Interval = new TimeSpan(0, 0, 0, 0, 100); //Cambiar aqui la velocidad en que envía el comando
            timerLog = new DispatcherTimer();
            timerLog.Tick += TimerLog_Tick;
            timerLog.Interval = new TimeSpan(0, 0, 0, 0, 10);
            log = "";
            timerLog.Start();
        }

        private void TimerLog_Tick(object sender, EventArgs e)
        {
            if (log != "")
            {
                txbLog.Text += "\n" + log;
                txbLog.ScrollToEnd();
                log = "";
            }
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            //Este método es el que envía el comando al puerto serie
            if (comando != "s")
            {
                puertoSerie.Escribir(comando);
            }
        }

        private void EscribirEnLog(string Objeto, string tipo, string mensaje)
        {
            log = tipo + "[" + Objeto + "]:" + mensaje;
        }
        

        protected override void OnClosing(CancelEventArgs e)
        {
            _videoSource.Stop();
            imgVideo.Source = null;
            base.OnClosing(e);
        }

        #region ManejoDeCamara

        private void btnIniciarDetener_Click(object sender, RoutedEventArgs e)
        {
            if (_videoSource.IsRunning)
            {
                _videoSource.Stop();
                imgVideo.Source = null;
                EscribirEnLog("Cámara", "Evento", "Detenida");
            }else
            {
                if (cmbOrigenes.SelectedItem != null)
                {
                    _videoSource = new VideoCaptureDevice((cmbOrigenes.SelectedItem as FilterInfo).MonikerString);
                    _videoSource.NewFrame += _videoSource_NewFrame;
                    _videoSource.Start();
                    EscribirEnLog("Cámara", "Evento", "Iniciada");
                }
            }
           
        }

        private void _videoSource_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            System.Drawing.Image img = (Bitmap)eventArgs.Frame.Clone();
            MemoryStream ms = new MemoryStream();
            img.Save(ms, ImageFormat.Bmp);
            ms.Seek(0, SeekOrigin.Begin);
            BitmapImage bi = new BitmapImage();
            bi.BeginInit();
            bi.StreamSource = ms;
            bi.EndInit();
            bi.Freeze();
            _frameActual = bi.Clone();
            Dispatcher.BeginInvoke(new ThreadStart(delegate
            {
                imgVideo.Source = bi;
            }));
        }

      

        private void btnTomarFoto_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string nombre = GeneraNombreDeArchivo();
                    var encoder = new JpegBitmapEncoder();
                    encoder.Frames.Add(BitmapFrame.Create((BitmapImage)_frameActual));
                    using (var filestream = new FileStream(nombre, FileMode.Create))
                    {
                        encoder.Save(filestream);
                    }
                EscribirEnLog("Cámara", "Evento", nombre);
                //MessageBox.Show(nombre);
            }
            catch (Exception ex)
            {
                EscribirEnLog("Cámara", "Error", ex.Message);
                //MessageBox.Show(ex.Message);
            }
        }
        private string GeneraNombreDeArchivo()
        {
            return DateTime.Now.ToString().Replace("/", "").Replace(" ", "").Replace(":", "").Replace(".", "") + ".jpg";
        }

        #endregion
        #region ManejoDePuertoSerie

        private void PuertoSerie_Evento(object sender, EventArgs e)
        {
            EscribirEnLog("Puerto Serie", "Evento:", sender.ToString());
        }



        private void PuertoSerie_DatoObtenido(object sender, string e)
        {
            EscribirEnLog("Puerto Serie", "Obtenido", e);
        }


        private void chkConectar_Checked(object sender, RoutedEventArgs e)
        {
            if (cmbPuerto.Items.Count > 0)
            {
                string puerto = cmbPuerto.Text;
                if (string.IsNullOrEmpty(puerto))
                {
                    EscribirEnLog("Puerto Serie", "Error", "No se ha seleccionado puerto serie");
                }
                else
                {
                    if (!puertoSerie.Conectado)
                    {
                        if (puertoSerie.Conectar(puerto))
                        {
                            timer.Start();
                            EscribirEnLog("Puerto Serie", "Evento", "Puerto serie conectado");
                        }
                        else
                        {
                            EscribirEnLog("Puerto Serie", "Error", "Error al conectar el puerto");
                        }

                    }
                    else
                    {
                        EscribirEnLog("Puerto Serie", "Error", "El puerto ya se encuentra en uso");
                    }
                }
            }
            else
            {
                EscribirEnLog("Puerto Serie", "Error", "No hay puertos disponibles");
            }
        }

        private void chkConectar_Unchecked(object sender, RoutedEventArgs e)
        {
            if (!puertoSerie.Conectado)
            {
                if (puertoSerie.Desconectar())
                {
                    timer.Stop();
                    EscribirEnLog("Puerto Serie", "Evento", "Puerto Serie desconectado");
                }
                else
                {
                    EscribirEnLog("Puerto Serie", "Error", "Error al desconectar el puerto");
                }
            }
        }

        private void btnDerecha_Click(object sender, RoutedEventArgs e)
        {
            comando = "r";
            if (puertoSerie.Conectado)
            {
                EscribirEnLog("Puerto Serie", "Evento", "Girando a la derecha");
            }
        }

        private void btnDetener_Click(object sender, RoutedEventArgs e)
        {
            comando = "s";
            if (puertoSerie.Conectado)
            {
                EscribirEnLog("Puerto Serie", "Evento", "Motor detenido");
            }
        }

        private void btnIzquierda_Click(object sender, RoutedEventArgs e)
        {
            comando = "l";
            if (puertoSerie.Conectado)
            {
                EscribirEnLog("Puerto Serie", "Evento", "Girando a la izquierda");
            }
        }

        private void btnEncenderLuzIzquierda_Click(object sender, RoutedEventArgs e)
        {
            puertoSerie.Escribir("A");
            if (puertoSerie.Conectado)
            {
                EscribirEnLog("Puerto Serie", "Evento", "Luz izquierda encendida");
            }
        }

        private void btnApagarLuzIzquierda_Click(object sender, RoutedEventArgs e)
        {
            puertoSerie.Escribir("a");
            if (puertoSerie.Conectado)
            {
                EscribirEnLog("Puerto Serie", "Evento", "Luz izquierda apagada");
            }
        }

        private void btnEncenderLuzDerecha_Click(object sender, RoutedEventArgs e)
        {
            puertoSerie.Escribir("B");
            if (puertoSerie.Conectado)
            {
                EscribirEnLog("Puerto Serie", "Evento", "Luz derecha encendida");
            }
        }

        private void btnApagarLuzDerecha_Click(object sender, RoutedEventArgs e)
        {
            puertoSerie.Escribir("b");
            if (puertoSerie.Conectado)
            {
                EscribirEnLog("Puerto Serie", "Evento", "Luz derecha apagada");
            }
        }
        #endregion
    }
}
