﻿using System;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using AForge.Video;
using AForge.Video.DirectShow;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;
using System.Windows.Threading;
using System.Threading;
using System.Windows;

namespace Tools
{
    public partial class ManejoCamaraWPF
    {
        private FilterInfoCollection _videoDevices;
        private VideoCaptureDevice _videoSource;
        private ComboBox _cmbCamaras;
        private System.Windows.Controls.Image _imgVisor;
        private BitmapImage _frameActual;
        private Window _ventana;
        public bool CamaraIniciada { get; set; }

        public ManejoCamaraWPF(System.Windows.Controls.Image visor, Window ventana)
        {
            _imgVisor = visor;
            CamaraIniciada = false;
        }

        public void LlenarConListaDeCamaras(ComboBox combo)
        {
            _cmbCamaras = combo;
            _videoDevices = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            _cmbCamaras.ItemsSource = _videoDevices;
            _cmbCamaras.DisplayMemberPath = "Name";
            _videoSource = new VideoCaptureDevice();
        }

        public void IniciarCamara()
        {
            if (_cmbCamaras.SelectedItem != null)
            {
                _videoSource = new VideoCaptureDevice((_cmbCamaras.SelectedItem as FilterInfo).MonikerString);
                _videoSource.NewFrame += _videoSource_NewFrame;
                _videoSource.Start();
                CamaraIniciada = true;
            }
        }
        public void DetenerCamara()
        {
            if (_videoSource.IsRunning)
            {
                _videoSource.Stop();
                _imgVisor.Source = null;
                CamaraIniciada = false;
            }
        }
        private void _videoSource_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            System.Drawing.Image img = (Bitmap)eventArgs.Frame.Clone();
            MemoryStream ms = new MemoryStream();
            img.Save(ms, ImageFormat.Bmp);
            ms.Seek(0, SeekOrigin.Begin);
            BitmapImage bi = new BitmapImage();
            bi.BeginInit();
            bi.StreamSource = ms;
            bi.EndInit();
            bi.Freeze();
            _frameActual = bi.Clone();
            _ventana.Dispatcher.BeginInvoke(new ThreadStart(delegate
            {
                _imgVisor.Source = bi;
            }));
        }

        public string TomarFoto(string ruta="")
        {
            try
            {
                if (CamaraIniciada)
                {
                    if (ruta == "")
                    {
                        ruta = GeneraNombreDeArchivo();
                    }
                    var encoder = new JpegBitmapEncoder();
                    encoder.Frames.Add(BitmapFrame.Create((BitmapImage)_frameActual));
                    using (var filestream = new FileStream(ruta, FileMode.Create))
                    {
                        encoder.Save(filestream);
                    }
                    return ruta;
                }
                else
                {
                    return "Cámara no iniciada";
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        private string GeneraNombreDeArchivo()
        {
            return DateTime.Now.ToString().Replace("/", "").Replace(" ", "").Replace(":", "").Replace(".", "") + ".jpg";
        }
    }
}
